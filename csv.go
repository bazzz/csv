package csv

import (
	"bytes"
	"compress/gzip"
	"encoding/csv"
	"errors"
	"fmt"
	"io"
	"net/http"
	"os"
	"strings"
)

// New reads the CSV data from path, which can be a file path or http(s) url, using separator and encoding to parse the data. The column names are expected to be on the first line. Field encoding can be an empty string.
func New(path string, separator rune, encoding string) (*Document, error) {
	if encoding != "" && encoding != "gzip" {
		return nil, errors.New("unsupported encoding: " + encoding)
	}
	var dataReader io.Reader
	if strings.HasPrefix(path, "http") {
		response, err := http.Get(path)
		if err != nil {
			return nil, fmt.Errorf("could not download csv data: %w", err)
		}
		defer response.Body.Close()
		dataReader = response.Body
	} else {
		fileData, err := os.ReadFile(path)
		if err != nil {
			return nil, fmt.Errorf("could not read csv data from file: %w", err)
		}
		dataReader = bytes.NewReader(fileData)
	}
	if encoding == "gzip" {
		gzipReader, err := gzip.NewReader(dataReader)
		if err != nil {
			return nil, fmt.Errorf("could not read gzip encoded data: %w", err)
		}
		defer gzipReader.Close()
		dataReader = gzipReader
	}
	return Parse(dataReader, separator)
}

// Parse uses separator to parse the CSV data from reader. The column names are expected to be on the first line.
func Parse(reader io.Reader, separator rune) (*Document, error) {
	csvReader := csv.NewReader(reader)
	csvReader.Comma = separator
	data, err := csvReader.ReadAll()
	if err != nil {
		return nil, fmt.Errorf("could not read csv data: %w", err)
	}
	return newDocument(data[0], data[1:]), nil
}
