package csv

type Document struct {
	headers []string
	rows    []Row
}

func newDocument(headers []string, data [][]string) *Document {
	rows := make([]Row, 0)
	for _, line := range data {
		row := make(map[string]string)
		for i, col := range headers {
			row[col] = line[i]
		}
		rows = append(rows, row)
	}
	return &Document{headers: headers, rows: rows}
}

// Headers returns a list of the existing headers.
func (d *Document) Headers() []string {
	return d.headers
}

// Rows returns the document data as a slice of Rows.
func (d *Document) Rows() []Row {
	return d.rows
}

type Row map[string]string

// Get return the value on this row in the column with the provided header name. If no such column exists, an empty string is returned.
func (r Row) Get(name string) string {
	if value, ok := r[name]; ok {
		return value
	}
	return ""
}
